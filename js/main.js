

$('#toggle').click(function() {
   $(this).toggleClass('active');
   $('#overlay').toggleClass('open');
});

$('#toggle-mob').click(function() {
   $(this).toggleClass('active');
   $('#overlay').toggleClass('open');
});

$('#primary-menu li a').click(function() {
   $('#toggle').toggleClass('active');
   $('#overlay').toggleClass('open');
});

$('#copySlider').owlCarousel({
    loop:true,
    nav:false,
    dots:true,
    animateOut: 'fadeOut',
    autoplay:false,
    items: 1,
    mouseDrag: false,
    autoHeight:true
});


$('#homecover').owlCarousel({
    loop:true,
    nav:false,
    dots:false,
    animateOut: 'fadeOut',
    autoplay:false,
    mouseDrag: false,
    responsive:{
        0:{
            items:1,
        },
        768:{
            items:1,
        },
        992:{
            items:1,
        }
    }
});

$('#home_slider_two--image').owlCarousel({
    loop:true,
    nav:false,
    dots:false,
    animateOut: 'fadeOut',
    autoplay:false,
    mouseDrag: false,
    responsive:{
        0:{
            items:1,
        },
        768:{
            items:1,
        },
        992:{
            items:1,
        }
    }
});

$('#home_slider_two--copy').owlCarousel({
    loop:true,
    nav:false,
    dots:true,
    animateOut: 'fadeOut',
    autoplay:false,
    items: 1,
    mouseDrag: false,
    autoHeight:true
});


var homecarousel = $('#homecover');
var copycarousel = $('#copySlider');

var carousel_home_img_two = $('#home_slider_two--image');
var carousel_home_cop_two = $('#home_slider_two--copy');

carousel_home_cop_two.on('change.owl.carousel', function(event) {
    if (event.namespace && event.property.name === 'position') {
      var target = event.relatedTarget.relative(event.property.value, true);
      carousel_home_img_two.owlCarousel('to', target, 300, true);
    }
  })

copycarousel.on('change.owl.carousel', function(event) {
    if (event.namespace && event.property.name === 'position') {
      var target = event.relatedTarget.relative(event.property.value, true);
      homecarousel.owlCarousel('to', target, 300, true);
    }
  })




  $('#copyMobSlider').owlCarousel({
    loop:true,
    nav:false,
    dots:true,
    animateOut: 'fadeOut',
    autoplay:false,
    items: 1,
    mouseDrag: false,
    autoHeight:true
});


$('#mobslide').owlCarousel({
    loop:true,
    nav:false,
    dots:false,
    animateOut: 'fadeOut',
    autoplay:false,
    responsive:{
        0:{
            items:1,
        },
        768:{
            items:1,
        },
        992:{
            items:1,
        }
    }
});

var mobcarousel = $('#mobslide');
var mobcopycarousel = $('#copyMobSlider');


mobcopycarousel.on('change.owl.carousel', function(event) {
    if (event.namespace && event.property.name === 'position') {
      var target = event.relatedTarget.relative(event.property.value, true);
      mobcarousel.owlCarousel('to', target, 300, true);
    }
  })

$('#home_slider_two--image-mob').owlCarousel({
    loop:true,
    nav:false,
    dots:false,
    animateOut: 'fadeOut',
    autoplay:false,
    responsive:{
        0:{
            items:1,
        },
        768:{
            items:1,
        },
        992:{
            items:1,
        }
    }
});


$('#home_slider_two--copy-mob').owlCarousel({
    loop:true,
    nav:false,
    dots:true,
    animateOut: 'fadeOut',
    autoplay:false,
    items: 1,
    mouseDrag: false,
    autoHeight:true
});


var carousel_home_img_two_mob = $('#home_slider_two--image-mob');
var carousel_home_cop_two_mob = $('#home_slider_two--copy-mob');

carousel_home_cop_two_mob.on('change.owl.carousel', function(event) {
    if (event.namespace && event.property.name === 'position') {
      var target = event.relatedTarget.relative(event.property.value, true);
      carousel_home_img_two_mob.owlCarousel('to', target, 300, true);
    }
});


$('#testimonials').owlCarousel({
    loop:true,
    nav:true,
    dots:true,
    responsive:{
        0:{
            items:1,
        },
        768:{
            items:1,
        },
        1200:{
            items:2,
        }
    }
});

//var toCross_position = $(".end-cover-page").offset().top;
var toCross_position = 60;

$(window).on("scroll", function() {

    if($(window).scrollTop() > toCross_position) {
        $(".site-header").addClass("active");
    } else {
       $(".site-header").removeClass("active");
    }
});


$('#see-more').on('click', function (e) {
  e.preventDefault()
  $('#v-pills-b').tab('show');
})


$( document ).ready(function() {

//   setTimeout(function() {
//       if(!sessionStorage.getItem('seeLetter')) {
//         $('#modal-founder').modal('show');
//       }
// }, 1000);

setTimeout(function() {
      $('#button-founders').show('slow');
}, 1000);

$('#modal-founder').on('hidden.bs.modal', function (e) {
    $('#button-founders').hide('slow');
})


// $('#modal-founder').on('shown.bs.modal', function (e) {
//   sessionStorage.setItem('seeLetter', true);
// })

$('.modalvideo').on('hidden.bs.modal', function (e) {
    $iframe = $(this).find("iframe");
    $iframe.attr("src", $iframe.attr("src"));
  });

});
