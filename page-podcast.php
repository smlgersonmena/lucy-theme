<?php
/**
 * Template name: Podcast
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Lucy_by_SML
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();
			?>
			<div class="w-100 py-3 mb-5 top-bar"></div>
			<div class="container-fluid">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<h1 class="tx-primary title text-center text-uppercase">
								<span class="line line-t"></span>
								<?php the_title(); ?>
								<span class="line line-b"></span>
							</h1>
						</div>
					</div>
				</div>
			</div>

			<div class="container-fluid py-4">
				<div class="container">
					<div class="row">
						<div class="col">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>


					<?php

						$argspods = array(
							'post_type' => 'podcast',
							'order' => 'ASC',
						);

						// The Query
						$the_querypods = new WP_Query( $argspods );

						// The Loop
						if ( $the_querypods->have_posts() ) {
							$count = 0;
							while ( $the_querypods->have_posts() ) {
								$the_querypods->the_post();
								?>

									<?php if ($count%2): ?>
										<div class="container-fluid py-4 bg-smoke">
											<div class="container">
										<div class="row py-3">

										<div class="col-12 offset-lg-4 col-lg-8">
											<div class="row">
												<div class="col-12 col-lg-6 text-center text-lg-right">
													<h5 class="text-calibre tx-wh-6 text-uppercase pt-3 tx-lts-xs"><?php the_title(); ?></h5>
													<p><?php the_excerpt() ?></p>
													<p class="mb-0 tx-ss">Duration: <?php the_field( "duration" ); ?></p>
												</div>
												<div class="col-12 col-lg-6">
													<div class="img-hexagon text-center">
														<a href="<?php the_permalink(); ?>">
														<div class="hexagon-clip hexagon-clip-color"></div>
														<div class="hexagon-clip hexagon-clip-img" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);"></div>
														</a>
													</div>
												</div>
											</div>
										</div>

									</div>
								</div>
							</div>


										<?php else: ?>

											<div class="container-fluid py-4">
												<div class="container">
											<div class="row py-3">

											<div class="col-12 col-lg-8">
												<div class="row">
													<div class="col-12 col-lg-6 order-1 order-lg-0">
														<div class="img-hexagon text-center">
															<a href="<?php the_permalink(); ?>">
															<div class="hexagon-clip hexagon-clip-color"></div>
															<div class="hexagon-clip hexagon-clip-img" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);"></div>
															</a>
														</div>
													</div>
													<div class="col-12 col-lg-6 order-0 order-lg-1 text-center text-lg-left">
														<h5 class="text-calibre tx-wh-6 text-uppercase pt-3 tx-lts-xs"><?php the_title(); ?></h5>
														<p><?php the_excerpt() ?></p>
														<p class="mb-0 tx-ss">Duration: <?php the_field( "duration" ); ?></p>
													</div>
												</div>
											</div>

										</div>
									</div>
								</div>
									<?php endif; ?>



								<?php
								$count++;
							}

						} else {
							// no posts found
						}
						/* Restore original Post Data */
						wp_reset_postdata();
						?>

			<?php
		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
