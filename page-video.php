<?php
/**
 *
 * Template name: Videos
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Lucy_by_SML
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();
			?>
			<div class="w-100 py-3 mb-5 top-bar"></div>
			<div class="container-fluid">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<h1 class="tx-primary title text-center text-uppercase">
								<span class="line line-t"></span>
								<?php the_title(); ?>
								<span class="line line-b"></span>
							</h1>
						</div>
					</div>
				</div>
			</div>

			<div class="container-fluid py-4">
				<div class="container">
					<div class="row">
						<div class="col">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>


						<?php
							$args = array(
								'post_type' => 'video',
								'order' => 'ASC',
							);

							// The Query
							$the_query = new WP_Query( $args );

							// The Loop
							if ( $the_query->have_posts() ) {
								$count = 0;
								while ( $the_query->have_posts() ) {
									$the_query->the_post();
									?>
									<?php if ($count%2):
										?>
										<div class="container-fluid py-4">
											<div class="container py-5">
												<div class="row justify-content-center">
													<div class="col-12 col-md-4">
														<h3 class="text-calibre tx-wh-6 text-uppercase pt-3 tx-lts-xs tx-primary"><?php the_title(); ?></h3>
														<p><?php the_content(); ?></p>
													</div>
													<div class="col-12 col-md-6 text-center">
														<a href="#modal-video-<?php the_ID(); ?>" data-toggle="modal" data-target="#modal-video-<?php the_ID(); ?>">
															<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" class="img-fluid">
														</a>
													</div>

												</div>
											</div>
											<!-- Modal -->
											<div class="modal fade modalvideo" id="modal-video-<?php the_ID(); ?>" tabindex="-1" role="dialog" aria-hidden="true">
												<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
													<div class="modal-content">
														<div class="modal-header">
															<h5 class="modal-title"><?php the_title(); ?></h5>
															<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true">&times;</span>
															</button>
														</div>
														<div class="modal-body">
															<div class="embed-responsive embed-responsive-16by9 video-iframe">
																<?php the_field( "video_link" ); ?>
															</div>
														</div>
													</div>
												</div>
											</div>

										</div>
										<?php else: ?>
											<div class="container-fluid  py-4">
												<div class="container">
													<div class="row justify-content-center">
														<div class="col-12 col-md-6 text-center order-1 order-lg-0">
															<a href="#modal-video-<?php the_ID(); ?>" data-toggle="modal" data-target="#modal-video-<?php the_ID(); ?>">
																<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" class="img-fluid">
																</a>
														</div>
														<div class="col-12 col-md-4 order-0 order-lg-1">
															<h3 class="text-calibre tx-wh-6 text-uppercase pt-3 tx-lts-xs tx-primary"><?php the_title(); ?></h3>
															<p><?php the_content(); ?></p>
														</div>
													</div>
												</div>
												<!-- Modal -->
												<div class="modal fade modalvideo" id="modal-video-<?php the_ID(); ?>" tabindex="-1" role="dialog" aria-hidden="true">
													<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title"><?php the_title(); ?></h5>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true">&times;</span>
																</button>
															</div>
															<div class="modal-body">
																<div class="embed-responsive embed-responsive-16by9 video-iframe">
																	<?php the_field( "video_link" ); ?>
																</div>
															</div>

														</div>
													</div>
												</div>
											</div>
										<?php
									endif; ?>

									<?php
									$count++;
								}


							} else {
								// no posts found
							}
							/* Restore original Post Data */
							wp_reset_postdata();
							?>

			<?php
		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
