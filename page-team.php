<?php
/**
 * Template name: Team
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Lucy_by_SML
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
        while (have_posts()) :
            the_post();
            ?>
			<div class="w-100 py-3 mb-5 top-bar"></div>
			<div class="container-fluid">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<h1 class="tx-primary title text-center text-uppercase">
								<span class="line line-t"></span>
								<?php the_title(); ?>
								<span class="line line-b"></span>
							</h1>
						</div>
					</div>
				</div>
			</div>

			<div class="container-fluid py-4">
				<div class="container">
					<div class="row">
						<div class="col">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>


					<?php
                        $argspods = array(
                            'post_type' => 'team_member',
                            'order' => 'ASC',
                        );

                        // The Query
                        $the_querypods = new WP_Query($argspods);

                        // The Loop
                        
                        if ($the_querypods->have_posts()) {
                            $count = 1; ?>
							<div class="container-fluid py-4 team-members">
								<div class="container">
									<div class="row py-3">
							<?php
                            while ($the_querypods->have_posts()) {
                                $isEven = '';
                                if ($count%2) {
                                    $isEven = 'tx-purple';
                                } else {
                                    $isEven = 'tx-primary';
                                }
								$the_querypods->the_post(); ?>
								
								<?php if ($count%3==0): ?>
									<div class="col-12 col-md-6 col-lg-4 p-3 my-2 item">
										<a href="<?php the_permalink(); ?>" class="h3 text-calibre tx-wh-6 text-uppercase pt-3 tx-lts-xs link-none <?php echo $isEven; ?>">
										<?php the_title(); ?>
									</a>
									<h6 class="text-uppercase text-calibre <?php echo $isEven; ?>"><?php the_field("position"); ?></h6>
									<p><?php the_content() ?></p>
								</div>
								<div class="col-12">
									<hr>
								</div>
								<?php else: ?>

									<div class="col-12 col-md-6 col-lg-4 p-3 my-2 item">
								<a href="<?php the_permalink(); ?>" class="h3 text-calibre tx-wh-6 text-uppercase pt-3 tx-lts-xs link-none <?php echo $isEven; ?>">
								<?php the_title(); ?>
							</a>
									<h6 class="text-uppercase text-calibre <?php echo $isEven; ?>"><?php the_field("position"); ?></h6>
									<p><?php the_content() ?></p>
								</div>

									<?php endif; ?>
								
							
								<?php
                                $count++;
                            } ?>
							</div>
								</div>
									</div>
							
							<?php
                        } else {
                            // no posts found
                        }
                        /* Restore original Post Data */
                        wp_reset_postdata();
                        ?>

			<?php
        endwhile; // End of the loop.
        ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
