<div class="container-fluid">
	<div class="container py-5">
		<div class="row justify-content-center">
			<div class="col-12 col-md-4">
				<div class="w-100 py-4">
					<h2 class="tx-primary title text-center text-uppercase">
						<span class="line line-t"></span>
						<?php esc_html_e( 'Testimonials', 'lucy-by-sml' ); ?>
						<span class="line line-b"></span>
					</h2>
				</div>
			</div>
		</div>
		<div class="row py-3">
			<div class="owl-carousel" id="testimonials">
				<?php

					$args = array(
						'post_type' => 'testimonial',
						'order' => 'ASC',

					);

					// The Query
					$the_query = new WP_Query( $args );

					// The Loop
					if ( $the_query->have_posts() ) {

						while ( $the_query->have_posts() ) {
							$the_query->the_post();
							?>
							<div class="col-10 col-md-11 text-left">
								<?php the_content(); ?>
								<h5 class="text-calibre tx-wh-6 text-uppercase"><?php the_title(); ?></h5>
								<p class="mb-0 tx-ss font-italic"><?php the_field( "subtitle" ); ?></p>
							</div>
							<?php
						}

					} else {
						// no posts found
					}
					/* Restore original Post Data */
					wp_reset_postdata();
					?>
			</div>
		</div>
	</div>
</div>
