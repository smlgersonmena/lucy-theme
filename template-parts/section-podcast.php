<div class="container-fluid bg-smoke">
	<div class="container py-5">
		<div class="row justify-content-center">
			<div class="col-12 col-md-5">
				<div class="w-100 py-4">
					<h2 class="tx-primary title text-center text-uppercase">
						<span class="line line-t"></span>
						<?php the_field( "title_3" ); ?>
						<span class="line line-b"></span>
					</h2>
				</div>
				<div class="w-100 text-center">
					<?php the_field( "copy_3" ); ?>
				</div>

			</div>
		</div>
		<div class="row py-4">
			<?php

				$argspods = array(
					'post_type' => 'podcast',
					'order' => 'ASC',
					'posts_per_page' => 3,
				);

				// The Query
				$the_querypods = new WP_Query( $argspods );

				// The Loop
				if ( $the_querypods->have_posts() ) {

					while ( $the_querypods->have_posts() ) {
						$the_querypods->the_post();
						?>
						<div class="col-12 col-lg-4 text-center item-podcast">
							<div class="img-hexagon mb-4">
								<a href="<?php the_permalink(); ?>">
								<div class="hexagon-clip hexagon-clip-color"></div>
								<div class="hexagon-clip hexagon-clip-img" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);"></div>
								</a>
							</div>
							<h5 class="text-calibre tx-wh-6 text-uppercase pt-3 tx-lts-xs"><?php the_title(); ?></h5>
							<div class="excerpt">
								<?php the_excerpt(); ?>
							</div>
							<p class="mb-0 tx-ss">Duration: <?php the_field( "duration" ); ?></p>
						</div>
						<?php
					}

				} else {
					// no posts found
				}
				/* Restore original Post Data */
				wp_reset_postdata();
				?>
		</div>
		<div class="row">
			<div class="col text-center">
				<a href="<?php the_permalink(39); ?>" class="bt bt-ghost bt-ghost_blue">View More</a>
			</div>
		</div>
	</div>
</div>
