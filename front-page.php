<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Lucy_by_SML
 */

get_header();
?>

<div id="primary" class="content-area">
    <main id="main" class="site-main">

        <?php
		if ( have_posts() ) :


			/* Start the Loop */
			while ( have_posts() ) :
				the_post();
?>

<div class="founders-button breathing" data-toggle="modal" data-target="#modal-founder" id="button-founders">

    <?php if ( get_field('img_founder') ) : ?>
        <img src="<?php the_field('img_founder'); ?>" alt="Founder letter">
    <?php else:?>
        <img src="<?php echo get_template_directory_uri(); ?>/img/lilian-hexagon.png" alt="Founder letter" class="">

    <?php endif; ?>
    
    
</div>

        <div class="container-fluid cover-page d-none d-lg-block">
            <div class="owl-carousel" id="homecover">
                <?php if ( have_rows('home_slider') ): ?>
                <?php while ( have_rows('home_slider') ) : the_row(); ?>
                <div class="item item_img" style="background-image: url(<?php the_sub_field('slide'); ?>)">
                    <div class="author">
                        <?php the_sub_field('author'); ?>
                    </div>
                </div>
                <?php endwhile; ?>
                <?php endif; ?>
            </div>

            <img src="<?php echo get_template_directory_uri(); ?>/img/cover-shape.svg" alt="Shape" class="shape">

            <div class="container py-5 d-flex align-items-center">
                <div class="row w-100 pt-5">
                    <div class="col-12 col-md-4 py-3">

                        <div class="owl-carousel text-white tx-md tx-w-5 text-center copySlider" id="copySlider">
                            <?php if ( have_rows('home_slider') ): ?>
                            <?php while ( have_rows('home_slider') ) : the_row(); ?>
                            <div class="w-100 px-2 d-flex flex-column align-items-center">
                                <h1 class="text-white title text-center text-calibre tx-lts-lg mb-4">
                                    <span class="line line-t"></span>
                                    <?php the_sub_field('title'); ?>
                                    <span class="line line-b"></span>
                                </h1>
                                <div class="w-100 tx-sx">
                                    <?php the_sub_field('copy'); ?>
                                </div>
                            </div>

                            <?php endwhile; ?>
                            <?php endif; ?>
                        </div>

                    </div>
                    <div class="scroll-down text-center pb-2 ">
                        <a href="#home_slider_two--image" class="w-100 d-block h-100">
                            <div class="arrowscroll arrowscroll-first"></div>
                            <div class="arrowscroll arrowscroll-second"></div>
                        </a>
                    </div>

                </div>
            </div>
        </div>

        <?php get_template_part( 'template-parts/section-mobcover'); ?>
        <?php get_template_part( 'template-parts/slider', 'woman'); ?>


        <!-- Modal -->
        <div class="modal fade" id="modal-founder" tabindex="-1" role="dialog" aria-labelledby="modal-founder" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="row d-xl-none">
                            <div class="col-12">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <h1 class="tx-primary title text-uppercase pb-3 text-center"><?php echo get_the_title(288); ?></h1>
                            </div>
                            <div class="col-12 img-founder" style="background-image: url(<?php echo get_the_post_thumbnail_url(288); ?>)">
                            
                            </div>
                            <div class="col-12 pt-3">
                            <div class="w-100 px-lg-3 mx-auto">
                                    <?php echo get_post_field('post_content', 288); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row d-none d-xl-flex">
                            <div class="col-12 col-lg-4 img-founder order-0" style="background-image: url(<?php echo get_the_post_thumbnail_url(288); ?>)">
                            </div>
                            <div class="col-12 col-lg-8 order-1 ">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <h1 class="tx-primary title text-uppercase pb-3 text-center"><?php echo get_the_title(288); ?></h1>
                                <div class="w-100 px-lg-3 mx-auto">
                                    <?php echo get_post_field('post_content', 288); ?>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid bg-smoke" id="who-is-lucy">
            <div class="container py-5">
                <div class="row">
                    <div class="col-12">
                        <h2 class="tx-primary title text-center text-uppercase">
                            <span class="line line-t"></span>
                            <?php the_field( "title_1" ); ?>
                            <span class="line line-b"></span>
                        </h2>
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col-12 col-lg-6 py-3">
                        <img src="<?php the_field( 'img_1' ); ?>" alt="Meet Lucy" class="img-fluid mx-auto px-lg-5">
                    </div>
                    <div class="col-10 col-md-11 col-lg-5 py-3">
                        <?php the_field( 'copy_1_a' ); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid bg-smoke" id="meet-lucy">
            <div class="container py-5">
                <div class="row">
                    <div class="col-12">
                        <h2 class="tx-primary title text-center text-uppercase">
                            <span class="line line-t"></span>
                                <?php the_field( "title_1_b" ); ?>
                            <span class="line line-b"></span>
                        </h2>
                    </div>
                </div>
                <div class="row align-items-center justify-content-center pt-4">
                    <div class="col-12 col-md-11 col-lg-5 py-3 order-1 order-lg-0">
                        <?php the_field( 'copy_1_b' ); ?>
                    </div>
                    <div class="col-12 col-lg-6 py-3 order-0 order-lg-1">
                        <div class="w-100">
                            <img src="<?php the_field( 'img_2' ); ?>" alt="Meet Lucy" class="img-fluid mx-auto px-lg-5">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php get_template_part( 'template-parts/section-video'); ?>

        <?php get_template_part( 'template-parts/section-podcast'); ?>

        <?php get_template_part( 'template-parts/section-testimonial'); ?>

        <div class="container-fluid bg-gradient tx-sm" id="join-lucy">
            <div class="container py-5">
                <div class="row justify-content-center text-white">
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="w-100 py-4">
                            <h2 class=" title text-center text-uppercase">
                                <span class="line line-t"></span>
                                <?php the_field( "title_4" ); ?>
                                <span class="line line-b"></span>
                            </h2>
                        </div>
                        <div class="w-100 text-center py-2">
                            <?php the_field( "copy_4_a" ); ?>
                        </div>
                        <div class="w-100">
                            <?php echo do_shortcode('[contact-form-7 id="5" title="Contact form join"]'); ?>
                        </div>
                        <div class="w-100 text-center py-2 link-white">
                            <?php the_field( "copy_4_b" ); ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>




        <?php
			endwhile;
		endif;
		?>

    </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();