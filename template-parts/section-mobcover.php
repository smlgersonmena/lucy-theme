<div class="container-fluid d-lg-none mobile-cover">
  <div class="owl-carousel" id="mobslide">
		<?php if ( have_rows('home_slider') ): ?>
			<?php while ( have_rows('home_slider') ) : the_row(); ?>
				<div class="item item_img" style="background-image: url(<?php the_sub_field('mobile_img'); ?>)">
				<?php if ( get_sub_field('author') ) : ?>
					<div class="author">
                        <?php the_sub_field('author'); ?>
                    </div>
				<?php endif; ?>
				</div>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
  <div class="gradient-square"></div>
	<div class="container d-flex align-items-end copy pb-3">
		<div class="row w-100 mx-auto">
			<div class="col-12 px-0">
			<div class="owl-carousel text-white tx-md tx-w-5 text-center copySlider" id="copyMobSlider">
                            <?php if ( have_rows('home_slider') ): ?>
                            <?php while ( have_rows('home_slider') ) : the_row(); ?>
							
                            <div class="w-100 px-2">
							<h1 class="text-white title text-center text-calibre tx-lts-lg mb-4">
                            <span class="line line-t"></span>
                            <?php the_sub_field('title'); ?>
                            <span class="line line-b"></span>
						</h1>
						<div class="tx-xsm mx-auto content-copy">

							<?php the_sub_field('copy'); ?>
						</div>
                            </div>

                            <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
				
			</div>
		</div>
	</div>
</div>
