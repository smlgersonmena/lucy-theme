var gulp = require("gulp"),
    sass = require("gulp-sass"),
    postcss = require("gulp-postcss"),
    autoprefixer = require("autoprefixer"),
    cssnano = require("cssnano"),
    sourcemaps = require("gulp-sourcemaps"),
    uglify = require("gulp-uglify"),
    touch = require('gulp-touch'),
    plumber = require('gulp-plumber'),
    browserSync = require("browser-sync").create();

    var paths = {
      styles: {
        src: "scss/*.scss",
        dest: "css/"
      },
      js: {
        src: "js/",
        dest: "js/dist/"
      }
    };

    function style() {
      return gulp
      .src(paths.styles.src)
      .pipe(plumber())
      // Initialize sourcemaps before compilation starts
      .pipe(sourcemaps.init())
      .pipe(sass())
      .on("error", sass.logError)
      // Use postcss with autoprefixer and compress the compiled file using cssnano
      .pipe(postcss([autoprefixer(), cssnano()]))
      // Now add/write the sourcemaps
      .pipe(sourcemaps.write())
      .pipe(gulp.dest(paths.styles.dest));
    }

exports.style = style;

// function jstask(){
//   return gulp
//             .src(paths.js.src)
//             .pipe(uglify())
//             .pipe(gulp.dest(paths.js.dest));
// }

gulp.task('default', function () {
    return gulp.watch(paths.styles.src, gulp.series('style'));
});
