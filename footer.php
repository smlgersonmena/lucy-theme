<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Lucy_by_SML
 */

?>

</div><!-- #content -->

<footer class="site-footer container-fluid py-3">
    <div class="container py-5">
        <div class="row justify-content-center">
            <?php if ( get_field('show_social_media_icons','options' ) ) : ?>
            <div class="col-12 py-3 py-lg-1 col-lg-3">
                <div class="w-80 px-5 mx-auto text-center">
                    <?php
						$custom_logo_id = get_theme_mod( 'custom_logo' );
						$url_logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
						?>
                    <img src="<?php echo $url_logo[0]; ?>" alt="" class="img-fluid logo">
                </div>

            </div>
            <div class="col-12 py-3 py-lg-1 col-lg-3 text-center text-lg-left">
                <a class="text-white d-block pb-3" href="<?php echo get_home_url(); ?>#join-lucy">Join Lucy</a>
                <a class="text-white d-block" href="<?php echo get_privacy_policy_url() ?>">Privacy Policy</a>
            </div>
            <div class="col-12 col-py-3 py-lg-1 col-lg-3">
                <?php if ( get_field('facebook', 'options') ) : ?>
                <a href="<?php the_field( 'facebook', 'options' ); ?>" target="_blank"
                    class="d-flex pb-3 text-white link-none justify-content-center justify-content-lg-start">
                    <i class="fa fa-facebook tx-lg pr-2"></i> <span>Facebook</span>
                </a>
                <?php endif; ?>
                <?php if ( get_field('instagram', 'options' ) ) : ?>

                <a href="<?php the_field( 'instagram', 'options' ); ?>" target="_blank"
                    class="d-flex pb-3 text-white link-none justify-content-center justify-content-lg-start">
                    <i class="fa fa-instagram tx-lg pr-2"></i> <span>Instagram</span>
                </a>
                <?php endif; ?>

                <?php if ( get_field('youtube', 'options' ) ) : ?>
                <a href="<?php the_field( 'youtube', 'options' ); ?>" target="_blank"
                    class="d-flex pb-3 text-white link-none justify-content-center justify-content-lg-start">
                    <i class="fa fa-youtube tx-lg pr-2"></i> <span>YouTube</span>
                </a>
                <?php endif; ?>

            </div>
            <div class="col-12 col-py-3 py-lg-1 col-lg-3">
                <?php if ( get_field('twitter', 'options') ) : ?>
                <a href="<?php the_field( 'twitter', 'options' ); ?>" target="_blank"
                    class="d-flex pb-3 text-white link-none justify-content-center justify-content-lg-start">
                    <i class="fa fa-twitter tx-lg pr-2"></i> <span>Twitter</span>
                </a>

                <?php endif; ?>
                <?php if ( get_field('linkedin', 'options') ) : ?>
                <a href="<?php the_field( 'linkedin', 'options' ); ?>" target="_blank"
                    class="d-flex pb-3 text-white link-none justify-content-center justify-content-lg-start">
                    <i class="fa fa-linkedin tx-h3 pr-2"></i> <span>LinkedIn</span>
                </a>
                <?php endif; ?>
                <?php if ( get_field('pinterest', 'options') ) : ?>
                <a href="<?php the_field( 'pinterest', 'options' ); ?>" target="_blank"
                    class="d-flex pb-3 text-white link-none justify-content-center justify-content-lg-start">
                    <i class="fa fa-pinterest tx-h3 pr-2"></i> <span>Pinterest</span>
                </a>
                <?php endif; ?>
            </div>
            <?php else:?>

            <div class="col-12 py-3 py-lg-1 col-lg-6">
                <div class="w-80 px-5 mx-auto text-center">
                    <?php
						$custom_logo_id = get_theme_mod( 'custom_logo' );
						$url_logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
						?>
                    <img src="<?php echo $url_logo[0]; ?>" alt="" class="img-fluid logo">
                </div>
                <a class="text-white text-center d-block py-3" href="<?php echo get_home_url(); ?>#join-lucy">Join Lucy</a>
                <a class="text-white text-center d-block" href="<?php echo get_privacy_policy_url() ?>">Privacy Policy</a>

            </div>
            <?php endif; ?>


        </div>
        <div class="row copyright">
            <div class="col text-center">
                <div class="divider text-center"></div>
                <p class="tx-xs text-white text-uppercase">© <?php echo date("Y"); ?> ALL RIGHTS RESERVED<br><a
                        href="http://smldesign.com.au/" target="_blank" class="text-white">Created by SML DESIGN</a></p>
            </div>
        </div>
    </div>
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>

</html>