<div class="container-fluid px-0 cover-page d-none d-lg-block">
    <div class="owl-carousel" id="home_slider_two--image">
        <?php if ( have_rows('home_slider_two') ): ?>
        <?php while ( have_rows('home_slider_two') ) : the_row(); ?>
        <div class="item item_img" style="background-image: url(<?php the_sub_field('slide'); ?>)">
            <div class="author">
                <?php the_sub_field('author'); ?>
            </div>
        </div>
        <?php endwhile; ?>
        <?php endif; ?>
    </div>

    <div class="container py-5 d-flex align-items-center">
        <div class="row w-100 pt-5">
            <div class="col-12 offset-md-8 col-md-4 py-3">

                <div class="owl-carousel text-white tx-md tx-w-5 text-center copySlider" id="home_slider_two--copy">
                    <?php if ( have_rows('home_slider_two') ): ?>
                    <?php while ( have_rows('home_slider_two') ) : the_row(); ?>
                    <div class="w-100 px-2 d-flex flex-column align-items-center">
                        <h1 class="text-white title text-center text-calibre tx-lts-lg mb-4">
                            <span class="line line-t"></span>
                            <?php the_sub_field('title'); ?>
                            <span class="line line-b"></span>
                        </h1>
                        <div class="w-100 tx-sx">
                            <?php the_sub_field('copy'); ?>
                        </div>
                    </div>

                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>

            </div>
            <div class="scroll-down text-center pb-2 ">
                <a href="#who-is-lucy" class="w-100 d-block h-100">
                    <div class="arrowscroll arrowscroll-first"></div>
                    <div class="arrowscroll arrowscroll-second"></div>
                </a>
            </div>

        </div>
    </div>
</div>

<div class="container-fluid d-lg-none mobile-cover">
  <div class="owl-carousel" id="home_slider_two--image-mob">
		<?php if ( have_rows('home_slider_two') ): ?>
			<?php while ( have_rows('home_slider_two') ) : the_row(); ?>
				<div class="item item_img" style="background-image: url(<?php the_sub_field('mobile_img'); ?>)">
				<?php if ( get_sub_field('author') ) : ?>
					<div class="author">
                        <?php the_sub_field('author'); ?>
                    </div>
				<?php endif; ?>
				</div>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
  <div class="gradient-square"></div>
	<div class="container d-flex align-items-end copy pb-3">
		<div class="row w-100 mx-auto">
			<div class="col-12 px-0">
			<div class="owl-carousel text-white tx-md tx-w-5 text-center copySlider" id="home_slider_two--copy-mob">
                            <?php if ( have_rows('home_slider_two') ): ?>
                            <?php while ( have_rows('home_slider_two') ) : the_row(); ?>
							
                            <div class="w-100 px-2">
							<h1 class="text-white title text-center text-calibre tx-lts-lg mb-4">
                            <span class="line line-t"></span>
                            <?php the_sub_field('title'); ?>
                            <span class="line line-b"></span>
						</h1>
						<div class="tx-xsm mx-auto content-copy">

							<?php the_sub_field('copy'); ?>
						</div>
                            </div>

                            <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
				
			</div>
		</div>
	</div>
</div>