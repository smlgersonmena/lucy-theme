<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Lucy_by_SML
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<meta name="theme-color" content="#060640">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
<?php if (is_front_page()){
	$classMenu = 'inHome';
}
	else {
		$classMenu = 'outHome';
	}?>
	<header id="masthead" class="site-header fixed-top <?php echo $classMenu; ?> d-none d-lg-block">
		<div class="container-fluid">
			<div class="row justify-content-between py-3">
				<div class="col-2 d-flex align-items-center">
					<div class="button_container" id="toggle">
						<span class="top"></span>
						<span class="middle"></span>
						<span class="bottom"></span>
						<p class="text-uppercase text-calibre d-none d-lg-block">Menu</p>
					</div>
				</div>
				<div class="col-6 col-md-2 site-branding">
					<?php
					$custom_logo_id = get_theme_mod( 'custom_logo' );
					$url_logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
					?>
					<div class="w-100 px-4 text-center">
						<a href="<?php echo get_home_url(); ?>"><img src="<?php echo $url_logo[0]; ?>" alt="" class="img-fluid logo"></a>
					</div>
				</div>
				<div class="col-2 d-flex align-items-center text-right lucy-btn">
					<a href="<?php echo get_home_url(); ?>#join-lucy" class="text-uppercase text-calibre" id="link-join-lucy"><span class="d-none d-lg-block">Join Lucy </span><i class="fa fa-heart-o tx-lg pl-2" aria-hidden="true"></i></a>
				</div>
			</div>
		</div>
	</header><!-- Desktop -->

	<header class="site-header d-lg-none mobile-header <?php echo $classMenu; ?>">
		<div class="container-fluid">
			<div class="row justify-content-between py-3">
				<div class="col-6 site-branding">
					<div class="w-100 pr-4">
						<a href="<?php echo get_home_url(); ?>"><img src="<?php echo $url_logo[0]; ?>" alt="" class="img-fluid logo"></a>
					</div>
				</div>
				<div class="col-6 d-flex align-items-center text-right lucy-btn">
					<div class="button_container mr-2" id="toggle-mob">
						<span class="top"></span>
						<span class="middle"></span>
						<span class="bottom"></span>
						<p class="text-uppercase text-calibre d-none d-lg-block">Menu</p>
					</div>
					<a href="<?php echo get_home_url(); ?>#join-lucy" class="text-uppercase text-calibre"><i class="fa fa-heart-o tx-lg pl-2" aria-hidden="true"></i></a>
				</div>
			</div>
		</div>
	</header><!-- Mobile -->

<div class="overlay" id="overlay">
	<nav class="overlay-menu main-navigation">
		<?php
				wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu'        => 'main',
					'menu_id'        => 'primary-menu',
					'container'       => false,
					'menu_class' => false,
				) );
		?>
	</nav>
</div>

	<div id="content" class="site-content">
