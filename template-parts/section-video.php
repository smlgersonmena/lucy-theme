<div class="container-fluid" id="video-section">
	<?php
	$img_left = get_field('img_left');
	$img_right = get_field('img_right');
	?>
	
	<img src="<?php echo esc_url($img_left['url']); ?>" alt="<?php echo esc_attr($img_left['alt']); ?>" class="video-thumb left d-none d-lg-block">
	<img src="<?php echo esc_url($img_right['url']); ?>" alt="<?php echo esc_attr($img_right['alt']); ?>" class="video-thumb right d-none d-lg-block">

	<?php if ( !empty( $img_left['caption'] ) ) : ?>
<div class="author author-left">
	<?php echo $img_left['caption'];  ?>
</div>
<?php endif; ?>
<?php if ( !empty( $img_right['caption'] ) ) : ?>
	<div class="author author-right">
		<?php echo $img_right['caption'];  ?>
	</div>
<?php endif; ?>

	<div class="row d-lg-none">
		<div class="col-6 px-0">
			<img src="<?php echo esc_url($img_left['url']); ?>" alt="<?php echo esc_attr($img_left['alt']); ?>" class="img-fluid">
		</div>
		<div class="col-6 px-0">
			<img src="<?php echo esc_url($img_right['url']); ?>" alt="<?php echo esc_attr($img_right['alt']); ?>" class="img-fluid">
		</div>
	</div>
	<div class="container py-5 d-flex align-items-center">
		<div class="row justify-content-center">
			<div class="col-12 col-md-8 col-lg-4">
				<div class="w-100 py-4">
					<h2 class="tx-primary title text-center text-uppercase">
						<span class="line line-t"></span>
						<?php the_field( "title_2" ); ?>
						<span class="line line-b"></span>
					</h2>
				</div>
				<div class="w-100 text-center">
					<?php the_field( "copy_2" ); ?>
				</div>
				<div class="w-100 text-center pt-3">
				<?php 
$link = get_field('link_video');
if( $link ): 
    $link_url = $link['url'];
    $link_title = $link['title'];
    $link_target = $link['target'] ? $link['target'] : '_self';
    ?>
    <a class="bt bt-ghost bt-ghost_blue" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
<?php endif; ?>

				</div>
			</div>
		</div>
	</div>
</div>
