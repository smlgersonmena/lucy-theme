<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Lucy_by_SML
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();
			?>
			<div class="w-100 py-3 mb-5 top-bar"></div>
			<div class="container-fluid">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<h1 class="tx-primary title text-center text-uppercase">
								<span class="line line-t"></span>
								<?php the_title(); ?>
								<span class="line line-b"></span>
							</h1>
						</div>
					</div>
				</div>
			</div>

			<div class="container-fluid py-4">
				<div class="container">
					<div class="row justify-content-center py-4">
						<div class="col-12 col-md-6">
							<div class="img-hexagon text-center">
								<div class="hexagon-clip hexagon-clip-img" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);"></div>
							</div>
						</div>
					</div>
					<div class="row justify-content-center pt-4">
						<div class="col-12 col-md-5 text-center">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>

			<div class="container-fluid py-4">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-12 col-md-8">
							<div class="embed-responsive embed-responsive-21by9">
								<?php the_field( "podcast_embed" ); ?>
							</div>
						</div>
					</div>
				</div>
			</div>


			<?php
		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
