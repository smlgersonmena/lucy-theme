<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Lucy_by_SML
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			?>

			<div class="w-100 py-3 mb-5 top-bar"></div>
			<div class="container-fluid">
				<div class="container">
					<div class="col-12">
						<h1 class="tx-primary title text-center text-uppercase">
							<span class="line line-t"></span>
							<?php the_title(); ?>
							<span class="line line-b"></span>
						</h1>
					</div>
				</div>
			</div>

			<div class="container-fluid py-4">
				<div class="container">
					<div class="row">
						<div class="col-12 col-lg-8 py-2">
							<?php the_content(); ?>
						</div>
						<div class="col-12 col-lg-4 py-2">
							<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" class="img-fluid">
						</div>
					</div>
				</div>
			</div>
<?php
		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
